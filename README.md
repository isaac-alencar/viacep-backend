# Projeto Via Cep - Back-End

  

### Sobre - visão geral do projeto

O projeto via cep consiste em uma aplicação web que busca informações sobre um dado cep. Esse projeto é parte do desafio referente a vaga na empresaa Eureka.

### Tecnologias Utilizadas

-  **Typescript** - superset javascript.

-  **Express** - framework para aplicativo da web do Node.js mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e móvel;

-  **Mongo** - Banco de dados não relacional;

-  **Jest** - biblioteca de testes voltada para javascript/typescript.

-  **Axios** - biblioteca que disponibiliza um client http para consultas;

-  **ts-node-dev** - biblioteca que permite executar um servidor Node.js escrito em typescript e observar alterações em tempo de dessenvolvimento;

-  **node-cache** - biblioteca que lida com a parte de cache em aplicações Node.js fornecendo uma forma simples de se trabalhar com essas operações.

### Como Executar o Projeto

Você vai precisar do mongo instalado na sua máquina ou uma versão
conteinerizada dele no Docker. Certifique-se disso. Conecte-se ao mongo
ou inicie o container;

**primeiro**: Você deve clonar o repositório e entrar no diretório do projeto;

**segundo**: Você deve abrir o projeto com seu editor de código;

**terceiro**: Você deve criar um arquivo `.env.development` e definir 
os atributos conforme o arquivo `.env.example` explica;

**por fim**: execute:
```bash

yarn

# ou caso você prefira

npm install # ou npm i

```

Depois que todas as dependências estiverem instaladas, execute:

```bash

yarn dev:server

# ou ainda, se preferir

npm run dev:server

```

### Como Executar os Testes

  

- Vá novamente ao terminal na pasta do projeto e execute:

```bash

yarn test

# ou 

npm test # ou npm t

```

## Rotas da aplicação

`[GET]` - /address/{zipcode}  - retorna as informações de um cep
- `zipcode`: deve seguir o padrão `xxxxx-xxx`
- `status`: 201;  200; 304; 400.

`[POST]` - /address/
- `status`: 201; 400; 500;
- `body` required* : 
```json
	{
		zipcode: "string",
		street: "string",
		city: "string",
		district: "string",
		uf: "string"
	}
``` 


`[PATCH]` - /address/**zipcode**
- `body` required* : 
- `status`: 201; 400; 500.
```json
  // Deve enviar um ou mais dos campos abaixo para ser atualizado;
	{
		"zipcode": "string",
		"street": "string",
		"city": "string",
		"district": "string",
		"uf": "string"
	}
``` 

`[DELETE]` - /address/**zipcode**
- `status`: 204; 400;