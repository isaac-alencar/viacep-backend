import { app } from "../../src/app";
import { agent as request } from "supertest";
import { AddressModel } from "../../src/models/Address";
import { disconnectDatabase } from "../../src/infra/database/init";

const server = app.listen(3333);

beforeEach(async () => {
  await AddressModel.deleteMany();
});

afterAll(async () => {
  await AddressModel.deleteMany();
  await disconnectDatabase();
  server.close();
  await new Promise<void>((resolve) => setTimeout(() => resolve(), 500));
});

describe("AddressController - [GET]/address/zipcode", () => {
  it("should return an address that was not created yet", async () => {
    const response = await request(server).get("/address/64016-610").send();

    const expectedData = {
      zipcode: "64016-610",
      street: "Rua Canadá",
      district: "Cidade Nova",
      city: "Teresina",
      uf: "PI",
    };

    expect(response.status).toBe(201);
    expect(response.body).toEqual(expect.objectContaining(expectedData));
  });

  it("should return an address that was already created with status 200", async () => {
    await request(server).get("/address/64016-610").send();
    const response = await request(server).get("/address/64016-610").send();

    expect(response.status).toBe(200);
    expect(response.body).toEqual(
      expect.objectContaining({
        zipcode: expect.any(String),
        street: expect.any(String),
        district: expect.any(String),
        city: expect.any(String),
        uf: expect.any(String),
      })
    );
  });
  it("should not return an address with an not existing zip code", async () => {
    const response = await request(server).get("/address/64016001").send();

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ error: "Mal formatted cep" });
  });
  it("should not return an address with mal formatted zip code", async () => {
    const response = await request(server).get("/address/64016aaa").send();

    expect(response.status).toBe(400);
    expect(response.body).toEqual({ error: "Mal formatted cep" });
  });
});

describe("AddressController - [POST]/address/zipcode", () => {
  it("should return new address from POST request", async () => {
    const expectedData = {
      zipcode: "64016-610",
      street: "Rua Canadá",
      district: "Cidade Nova",
      city: "Teresina",
      uf: "PI",
    };

    const response = await request(server).post("/address/").send(expectedData);

    expect(response.status).toBe(201);
    expect(response.body).toEqual(expect.objectContaining(expectedData));
  });
});

describe("AddressController - [PATCH]/address/zipcode", () => {
  it("should return new address from POST request", async () => {
    const expectedData = {
      zipcode: "64016-610",
      street: "Rua Canadá",
      district: "Cidade Nova",
      city: "Teresina",
      uf: "PI",
    };
    await request(server).post("/address/").send(expectedData);

    const response = await request(server)
      .patch(`/address/${expectedData.zipcode}`)
      .send({
        district: "Cidade Velha",
      });

    expect(response.status).toBe(201);
    expect(response.body).toEqual(
      expect.objectContaining({ ...expectedData, district: "Cidade Velha" })
    );
  });
});

describe("AddressController - [DELETE]/address/zipcode", () => {
  it("should return new address from POST request", async () => {
    const expectedData = {
      zipcode: "64016-610",
      street: "Rua Canadá",
      district: "Cidade Nova",
      city: "Teresina",
      uf: "PI",
    };
    await request(server).post("/address/").send(expectedData);

    const response = await request(server)
      .delete(`/address/${expectedData.zipcode}`)
      .send();

    expect(response.status).toBe(204);
  });
});
