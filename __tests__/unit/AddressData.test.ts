import { AddressData } from "../../src/data/AddressData";
import { fakeAddressGenerator } from "../fixtures/FakeAddressGenerator";

afterAll(async () => {
  await new Promise<void>(resolve => setTimeout(() => resolve(), 500));
})

describe("AddressData - Testing database access layer", () => {
  const addressData = new AddressData();
  const fakeAddress = fakeAddressGenerator();

  it("should create new address", () => {
    addressData.create(fakeAddress).then((createdAddress) => {
      expect(createdAddress).toHaveProperty("_id");
      expect(createdAddress).toEqual(expect.objectContaining(fakeAddress));
    });
  });
  it("should return address", () => {

    addressData.create(fakeAddress).then((createdAddress) => {
      addressData.find(createdAddress.id).then((address) => {
        expect(address).toHaveProperty("zipcode");
        expect(address).toEqual(expect.objectContaining(fakeAddress));
      });
    });

  });
  it("should update address", () => {
    addressData.create(fakeAddress).then((createdAddress) => {
      addressData.update(createdAddress.id, { zipcode: '00000-000' }).then((address) => {
        expect(address).toHaveProperty("zipcode", '00000-000');
        expect(address).toEqual(expect.objectContaining(fakeAddress));
      });
    });
  });
  it("should delete address", () => {
    addressData.create(fakeAddress).then((createdAddress) => {
      addressData.delete(createdAddress.id).then((address: any) => {
        expect(address.status).toBe(200);
      });
    });
  });
});
