function fakeAddressGenerator() {
  const sixIntegerParts = 100000;
  const threeIntegerParts = 1000;

  /* 
    String(Math.random() * 100000) returns a number as 10000.794686620815
    in the order of hundredths of a thousand before the float point. 

    On split, this pseudo-aleatory number that we get, is only used the 
    six integer numbers like 100000 on previous example.

    the same thing after `-` character but with three integer numbers

  */
  const zipCodeGen = `${String(Math.random() * sixIntegerParts).split(".")[0]}-${
    String(Math.random() * threeIntegerParts).split(".")[0]
  }`;

  const stringGen = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

  return {
    zipcode: zipCodeGen,
    street: stringGen,
    district: stringGen,
    city: stringGen,
    uf: stringGen,
  };
}

export { fakeAddressGenerator };
