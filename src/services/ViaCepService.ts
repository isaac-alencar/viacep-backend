import axios from "axios";

class ViaCepService {
  async queryZipCode(zipcode: string) {
    try {
      const response = await axios({
        url: `https://viacep.com.br/ws/${zipcode}/json/`,
        method: "GET",
      });

      if (response.data.erro) {
        return "Error! check the zip code you provide";
      }

      return response.data;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }
}

export { ViaCepService };
