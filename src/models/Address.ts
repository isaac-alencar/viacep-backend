import { Schema, model } from "mongoose";

const AddressSchema = new Schema({
  zipcode: {
    type: String,
    required: true,
  },
  street: {
    type: String,
    required: true,
  },
  district: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  uf: {
    type: String,
    required: true,
  },
});

const AddressModel = model("Address", AddressSchema);

export { AddressModel };
