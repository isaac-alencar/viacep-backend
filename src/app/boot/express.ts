import express, { Application } from "express";
import cors from 'cors';
import { routes } from "../../routes/routes";

function boot(app: Application) {
  app.use(cors());
  app.use(express.json());
  app.use(routes);
}

export { boot };
