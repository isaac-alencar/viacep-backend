import { AddressModel } from "../models/Address";

export interface IAddressData {
  zipcode: string | undefined;
  street: string | undefined;
  district: string | undefined;
  city: string | undefined;
  uf: string | undefined;
}

class AddressData {
  async find(zipcode: string) {
    const address = await AddressModel.findOne({ zipcode }).exec();

    return address;
  }

  async create(addressData: IAddressData | undefined) {
    const address = await AddressModel.create(addressData);
    address.save(function (error) {
      if (error) throw new Error(`Error: ${error}`);
    });

    return address;
  }

  async update(zipcode: string, field: object | undefined) {
    const updatedAddress = await AddressModel.findOneAndUpdate({
      $where: zipcode
    }, field, { new: true });

    return updatedAddress;
  }

  async delete(zipcode: string) {
    await AddressModel.deleteOne({
      $where: zipcode
    }).exec()

    return;
  }

}

export { AddressData };
