import NodeCache from "node-cache";
import { AddressData } from "../data/AddressData";
import { ViaCepService } from "../services/ViaCepService";

class CacheUtil {
  private nodeCache = new NodeCache();
  private viaCepService = new ViaCepService();

  async getAddressFromAPI(zipcode: string) {
    try {
      const address = await this.viaCepService.queryZipCode(zipcode);

      return address;
    } catch (error) {
      throw new Error(`Error: ${error}`);
    }
  }

  async getAddressFromCache(key: string) {
    const addressAlreadyCached = this.nodeCache.has(key);
    if (addressAlreadyCached) {
      return this.nodeCache.get(key);
    }

    return;
  }

  setAddressOnCache(key: string, value: object) {
    const timeToExpiresCache = 36000; // one hour in seconds
    this.nodeCache.set(key, value, timeToExpiresCache);
    return;
  }
}

export { CacheUtil };
