import { Request, Response, Router } from "express";
import { AddressController } from "../controllers/AddressController";

const routes = Router();

routes.get("/address/:zipcode", (req: Request, res: Response) =>
  new AddressController(req, res).show()
);
routes.post("/address/", (req: Request, res: Response) =>
  new AddressController(req, res).store()
);
routes.patch("/address/:zipcode", (req: Request, res: Response) =>
  new AddressController(req, res).update()
);
routes.delete("/address/:zipcode", (req: Request, res: Response) =>
  new AddressController(req, res).delete()
);

export { routes };
