import mongoose from "mongoose";
import { databaseUrl, databaseOptions } from "../config/database";

const initDatabase = () => {
  try {
    mongoose.connect(databaseUrl, () => {
      databaseOptions;
    });
    console.log("Connected with database");
  } catch (error) {
    console.log("Error while connect with database");
  }
};

const disconnectDatabase = async () => {
  try {
    await mongoose.disconnect()
    console.log("Disconnected with database");
  } catch (error) {
    console.log("Error while Disconnect with database");
  }
};

export { initDatabase, disconnectDatabase };
