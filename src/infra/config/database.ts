// Database configuration

const databaseUrl = `${process.env.MONGO_URL}`;

const databaseOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export { databaseOptions, databaseUrl };
