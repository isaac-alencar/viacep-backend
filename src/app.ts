import "./infra/config/dotenv";

import express from 'express';
import { boot } from './app/boot/express';
import { initDatabase }  from './infra/database/init';

const app = express();

initDatabase();
boot(app);

export { app };
