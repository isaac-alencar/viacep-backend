import { Request, Response } from "express";
import { AddressData, IAddressData } from "../data/AddressData";
import { CacheUtil } from "../utils/cache";

class AddressController {
  private addressData = new AddressData();
  private cacheUtil = new CacheUtil();

  req!: Request;
  res!: Response;
  constructor(req: Request, res: Response) {
    Object.assign(this, {
      req,
      res,
    });
  }

  async show() {
    const { zipcode } = this.req.params;

    zipcode.trim();

    const validZipCodeFormat = zipcode.match(/^\d{5}-\d{3}$/);
    if (!validZipCodeFormat)
      return this.res.status(400).json({ error: "Mal formatted cep" });

    try {
      const addressOnCache = await this.cacheUtil.getAddressFromCache(zipcode);

      if (!addressOnCache) {
        const existingAddressOnDB = await this.addressData.find(zipcode);
        if (existingAddressOnDB) {
          this.cacheUtil.setAddressOnCache(zipcode, existingAddressOnDB);
          return this.res.status(200).json(existingAddressOnDB);
        }

        const { cep, logradouro, bairro, localidade, uf } =
          await this.cacheUtil.getAddressFromAPI(zipcode);

        const address = await this.store({
          zipcode: cep,
          street: logradouro,
          district: bairro,
          city: localidade,
          uf,
        });

        return address;
      }
      return this.res.status(304).json(addressOnCache);
    } catch (error) {
      return this.res.status(400).send(error);
    }
  }

  async store(data?: IAddressData) {
    let newAddress = data ? data : this.req.body;

    if (!newAddress) {
      return this.res
        .status(400)
        .json({ error: "Missing fields to create address" });
    }

    const address = await this.addressData.create(newAddress);

    if (!address)
      return this.res.status(500).json({ error: "Error on creating address" });

    return this.res.status(201).json(address);
  }

  async update(field?: object) {
    field = field ? field : this.req.body;
    const { zipcode } = this.req.params;

    if (!zipcode) {
      return this.res
        .status(400)
        .json({ error: "You must provide the zipcode" });
    }

    const updatedAddress = await this.addressData.update(zipcode, field);

    if (!updatedAddress)
      return this.res
        .status(500)
        .json({ error: "Error while updating address" });

    return this.res.status(201).json(updatedAddress);
  }

  async delete() {
    const { zipcode } = this.req.params;

    if (!this.req.params) {
      this.res.status(400).json({ error: "You must provide the zipcode" });
    }

    await this.addressData.delete(zipcode);

    return this.res.status(204).send();
  }
}

export { AddressController };
